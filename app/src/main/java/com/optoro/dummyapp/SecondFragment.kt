package com.optoro.dummyapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.RestrictionsManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.optoro.dummyapp.databinding.FragmentSecondBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var restrictionManager: RestrictionsManager? = null
    private val restrictionsFilter = IntentFilter(Intent.ACTION_APPLICATION_RESTRICTIONS_CHANGED)
    private val restrictionsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            processRestrictions()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        restrictionManager = requireActivity().getSystemService(Context.RESTRICTIONS_SERVICE) as RestrictionsManager
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        processRestrictions()
        activity?.let { context ->
            ContextCompat.registerReceiver(context, restrictionsReceiver,
                restrictionsFilter, ContextCompat.RECEIVER_EXPORTED)
        }
    }

    override fun onPause() {
        super.onPause()
        activity?.unregisterReceiver(restrictionsReceiver)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun processRestrictions() {
        activity?.runOnUiThread {
            val restrictions = restrictionManager?.applicationRestrictions
            val key = "helloWorldName"

            if (restrictions?.containsKey(key) == true) {
                binding.textviewSecond.text =
                    getString(R.string.hello_second_greeting, restrictions.getString(key))
            } else {
                binding.textviewSecond.text = getString(R.string.hello_second_no_name)
            }

            if (restrictions?.containsKey("justARandomNumber") == true) {
                binding.textviewRandomNumber.text = restrictions.getInt("justARandomNumber", -1).toString()
            } else {
                binding.textviewRandomNumber.text = getString(R.string.hello_second_no_random_number)
            }
        }
    }
}